# OM

[![Build Status](https://api.travis-ci.org/Kedrigern/omnew.svg)](https://travis-ci.org/Kedrigern/omnew)

Webové stránky [Otevřená města, z. s.](https://www.otevrenamesta.cz/)

## Lokalní spuštění ([Jekyll](https://en.wikipedia.org/wiki/Jekyll_(software)))

```#bash -vypuštěno, neboť neumožňuje rozlišení mezí přihlášením jako # a $. U příkazů z CMD to ostatně nedává moc smysl.
$ cd <dir otevrenamesta.cz>
$ npm install
$ bundle install --path vendor/bundle
$ bundle exec jekyll serve --livereload --future --drafts
```

Stránky pak zobrazíte v prohlížeči na adrese ``http://localhost:4000``. Uložené změny se ihned promítnou.

## Instalace

[Fedora](https://cs.wikipedia.org/wiki/Fedora):

```#bash
# dnf install gcc rubygem-bundler ruby-devel rubygem-jekyll rubygem-nokogiri redhat-rpm-config

$ cd <dir otevrenamesta.cz>
$ bundle install --path vendor/bundle
$ bundle exec jekyll serve --livereload --future --drafts
```

..tedy včetně spuštění Jekyllu.