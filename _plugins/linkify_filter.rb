module Jekyll
  module LinkifyFilter

    def linkify(text)
      return '' if text.nil? || text.empty?

      "<a href=\"#{text}\">#{text}</a>"
    end
  end
end

Liquid::Template.register_filter(Jekyll::LinkifyFilter)
