module Jekyll
  class ProjektyGenerator < Generator
    safe true

    def generate(site)
      dir = 'projekty'
      site.data['projekty']['software'].each do |projekt|
        next unless projekt['slug']

        site.pages << ProjektPage.new(site, site.source, dir, projekt)
      end
    end
  end

  class ProjektPage < Page
    def initialize(site, base, dir, projekt)
      @site = site
      @base = base
      @dir = dir
      @name = projekt['slug'] + ".html"

      process(@name)
      read_yaml(File.join(base, '_layouts'), 'projekt.html')
      data['projekt'] = projekt
      data['title'] = projekt['title']
    end
  end
end