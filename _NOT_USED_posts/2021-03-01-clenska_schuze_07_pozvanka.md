---
layout:       blog
image:        clenska_2020_online.jpg
title:        Pozvánka na členskou schůzi dne 5.3.2021
hidden:       false
---
Vážení členové spolku Otevřená města,  

dovolte nám, abychom Vás jménem výboru Otevřených měst pozvali na konání 1. členské  schůze v roce 2021, která se bude konat v pátek 5. 3. 2021 od 9 do 12 hod. Členská schůze  bude zejména o vizi, strategickém směřování spolku v následujících měsících a diskusi Vašich  aktuálních potřeb.
 <!--more-->

**Program členské schůze:**

  - 9:00-9:15 Zahájení a představení účastnících se členů
  - 9:15-10:00 Představení Strategie Otevřených měst 2021-2022 s výhledem do 2026/2027 + diskuse aktuálních potřeb členů
  - 10:00-10:30 Představení návrhu změny stanov Otevřených měst zejména ve spojitosti  s novou možnosti vstupu krajů do spolku a dalších organizačních úprav +  Aktualizace smlouvy o výkonu funkce
  - 10:30-11:00 Představení kandidáta na pozici předsedy Otevřených měst 11:00-11:30 Hlasování
  - 11:30-12:00 Závěrečná diskuse

**Technické zajištění členské schůze:**
Schůze proběhne na platformě Google Meet.  
Adresa schůzky je: [https://meet.google.com/nrf-hnhn-yya](https://meet.google.com/nrf-hnhn-yya)    
Hlasování proběhne formou zaslání zprávy do chatu schůzky.    
Celá členská schůze bude nahrávána, připojením se na schůzku s nahráváním souhlasíte.  

**Podklady pro jednání:**
1. Návrh strategie Otevřených měst 2021-2022 s výhledem do 2026/2027
2. Dotazníkové šetření aktuálních potřeb členů (formulář zaslaný společně s pozvánkou  viz níže)
3. Podklady pro změnu stanov Otevřených měst (ve formě bodů k diskusi)
4. Podklady od kandidáta na pozici předsedy Otevřených měst
5. Aktualizovaná verze smlouvy o výkonu funkce ke schválení
6. Rozpočet za rok 2020
7. Výzva pro členy o návrh kandidátů na uvolněné místo do výboru OM za pana Víta  Olmra.

Prosíme o vyplnění krátkého dotazníkového šetření na adrese:
[https://forms.gle/BhYSnHJPsK9kRjE99](https://forms.gle/BhYSnHJPsK9kRjE99)  

Vaše odpovědi nám umožní lepší přípravu na členskou schůzi a bude sloužit jako podklad pro  diskusi o strategickém směřování organizace a Vašich potřebách jako členů Otevřených měst.

Děkujeme a těšíme se na setkání s Vámi.

Jiří Marek  
předseda Otevřených měst, z.s.

Malinovského náměstí 624/3  
602 00 Brno  

(+420) 602 570 184  
jiri.marek@otevrenamesta.cz

V Brně dne 18. 2. 2021

- [Oficiální pozvánka (PDF)](https://gitlab.com/otevrenamesta/documents/-/blob/master/schuze/2021_03_05/pozvanka_2021-02-02.pdf)
- [Oficiální pozvánka (DOCX)](https://docs.google.com/document/d/1hYCt8E7PzHupVUYpPSuaNmtpBcNSZqyz/edit)
