---
layout:       blog
image:        logo-otevrena-mesta.png
title:        Informace o členské schůzi dne 5. 3. 2021
hidden:       false
---
Zveme členy, zájemce o členství i ostatní příznivce na členskou schůzi, která proběhne v pátek **5.&nbsp;března&nbsp;2021** online.

 <!--more-->

Vážení členové spolku Otevřená města,

dovolte mi, abych Vás informoval jménem výboru Otevřených měst o datu konání 1. členské
schůze v roce 2021, která se bude konat v pátek 5. 3. 2021 od 9 do 12 hod. Členská schůze
bude zejména o vizi, strategickém směřování spolku v následujících měsících a diskusi Vašich
aktuálních potřeb.

Rámcový program bude tento:

1. Představení Strategie Otevřených měst 2021-2022
2. Představení návrhu změny stanov Otevřených měst zejména ve spojitosti s novou
   možnosti vstupu krajů do spolku a dalších organizačních úprav
3. Dovolení výboru Otevřených měst, jak bylo slíbeno na poslední členské schůzi
4. Diskuse na téma Vašich aktuálních potřeb.

Dne 18. 2. 2021 obdržíte pozvánku s oficiálním programem, harmonogramem diskuse,
informací o platformě pro konání členské schůze (členská schůze proběhne distančně) a
související podklady.

Dle jednacího řádu Vás tímto jako člena spolku vyzývám k zaslání Vašich návrhů do programu
členské schůze. Návrhy zasílejte nejpozději do 17. 2. 2021 na adresu info@otevrenamesta.cz.

Děkujeme a těšíme se na setkání s Vámi.

Jiří Marek
předseda Otevřených měst, z.s.

Malinovského náměstí 624/3  
602 00 Brno  
(+420) 602 570 184  
jiri.marek@otevrenamesta.cz

V Brně dne 2. 2. 2021


- [Seznam členů a jejich zástupců](http://www.otevrenamesta.cz/clenstvi)
- [Oficiální pozvánka](https://gitlab.com/otevrenamesta/documents/-/blob/master/schuze/2021_03_05/pozvanka_2021-02-02.pdf)
