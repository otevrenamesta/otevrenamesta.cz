---
layout:       blog
image:        logo-otevrena-mesta.png
title:        Rok 2021 jako příležitost pro otevřené inovace v oblasti digitalizace samospráv
hidden:       false
---
Tisková zpráva

 <!--more-->

**Spolek Otevřená města, z.s** prosazuje otevřené fungování veřejné správy v nejširším slova smyslu od základní formy transparentnosti ve formě zveřejňování informací ve formátu otevřených dat, přes průhledné veřejné zakázky, rozhodování na základě faktů včetně zapojování veřejnosti a v neposlední řadě používání otevřených řešení ve veřejné správě.

Na podzim roku 2020 došlo k rozšíření témat, kterým se Otevřená města začala věnovat. V obecné rovině jsme tak přešli od spolku zaměřujícího se jen na Open Source tvorbu na spolek, který otevřená řešení vnímá jako součást profesionálního konsultingu v oblasti digitalizace samospráv. Zavedli jsme jasný princip: “as open as possible, as closed as necessary” (co nejotevřenější, uzavřené dle potřeby). Je tak pro nás důležité zejména otevřené ale zároveň i efektivní řešení.

Otevřená města do roku 2021 tak vstupují následujícím směrem:

  1. **Co jsme/cheme být v OM 2.0?** Profesionální „konsulting“ s prvky „Software House“ pro oblast digitalizace samospráv.
  2. **Co nabízíme/chceme nabízet?** Nabízíme technickou, právní a manažerskou expertízu v oblasti digitalizace samospráv.
  3. **Kam směřujeme?** K tomu být partner samospráv při digitalizaci na různých úrovních.

Začali jsme tak pracovat na tématech jako je:

  1. centrální zadávání veřejných zakázek zejména na dodávky otevřených technologií pro výkon veřejné správy,
  2. sdílení expertů a know-how z technických, právních a manažerských oblastí,
  3. společné zapojení do národních i mezinárodních grantových soutěží souvisejících s tématy spolupráce,
  4. společné pořádání akcí a rozvoj otevřených komunit.

Brzy se opět ozveme s novinkami z řad strategických partnerství, které se nám podařilo domluvit a dále Vám sdělíme informacemi o chystané členské schůzi na únor 2021.

Na závěr je důležité poznamenat, že na začátku roku naše řady rozšířily tři nové obce: Kroměříž, Bystřice a Moravská Třebová a aplikaci Cityvizor začalo nasazovat Statutární město Ostrava.


V Brně dne 18.1.2021

Výbor Otevřených měst
