---
layout:       blog
image:        clenska_2020_ZS_Amos.jpg
title:        Členská schůze Otevřených měst &num;5 | Dolní Jirčany
hidden:       false
---
Zveme členy, zájemce o členství i ostatní příznivce na členskou schůzi, která proběhne v úterý **15.&nbsp;září&nbsp;2020** v Dolních Jirčanech.

 <!--more-->

Členská schůze se uskuteční v čase od **10:00 do 16:00** v prostorech **[ZŠ Amos](https://www.zsamos.cz/)**, **[Pražská 1000](https://www.openstreetmap.org/?mlat=49.95003&mlon=14.51796#map=17/49.95002/14.51796&layers=N)**, Dolní Jirčany.

Program jednání bude odsouhlasen účastníky na začátku schůze. Předpokládaný průběh je následující:

bod | kdy | co | zpravodaj
--- | --: | --- |
|09:00|otevření místnosti|
|09:30|začátek registrace účastníků|
|09:55|konec registrace účastníků|
1|10:00|schválení programu schůze|členové
2|10:10|zpráva předsedy o činnosti|L. Nešněra
3|10:45|volba výboru a předsedy ([požadavky](https://www.otevrenamesta.cz/stanovy#8.3))|členové
|11:45|oběd|
4|12:45|rozprava: budoucnost spolku (produktové řízení, transparentnost, plány, spolupráce s kraji, ..)|členové
|14:30|pauza|
5|14:45–16:00|prezentace projektů:|
|(14:45)|&nbsp;&nbsp;&nbsp;[CityVizor](/projekty/cv)|Pavla Kadlecová, Martin Šebek
|(15:00)|&nbsp;&nbsp;&nbsp;[DSW2](/projekty/dsw)|Praha 3 + Marek Sebera
|(15:15)|&nbsp;&nbsp;&nbsp;[Gisquick](/projekty/GISEditor)|Jáchym Čepický
|(15:30)|&nbsp;&nbsp;&nbsp;infrastruktura spolku a přenositelnost na obce|L. Nešněra
|(15:45)|&nbsp;&nbsp;&nbsp;ostatní|
6|16:00|komentovaná prohlídka [hi-tech multifunkční školy](http://www.earch.cz/cs/architektura/zakladni-skola-amos-od-soa-architekti-slouzi-zaroven-jako-komunitni-centrum-obce-psary) - nic není zábavněší, než realita viděná s odstupem|Vít Olmr

Členské schůze se bude možné účastnit také s využitím **videokonference** [Jitsi](https://en.wikipedia.org/wiki/Jitsi) na adrese [https://meet.vpsfree.cz/OM2020](https://meet.vpsfree.cz/OM2020). **Vzdáleně hlasovat**:
- bude možné pouze pomocí [sítě Tox](https://cs.wikipedia.org/wiki/Tox)
- žádáme členy, kteří vzdálenou účast plánují, aby nejpozději do 13. září 2020 zaslali zprávou z oficiálního účtu datových schránek člena Tox ID, které zástupce použije
- klienta pro Tox můžete stáhnout na adrese [https://tox.chat/download.html](https://tox.chat/download.html)
- základní návod je k dispozici na adrese [https://cs.wikibooks.org/wiki/Tox](https://cs.wikibooks.org/wiki/Tox)

Také si dovolujeme upozornit na skutečnost, že hlasovat mohou pouze osoby, které doložily nebo nejpozději na členské schůzi doloží své oprávnění zastupovat člena na jednání spolku. Netýká se osob, které jsou zastupujícími dle Zákona o obcích (např. starosta, místostarosta, . . . ), nebo byly uvedeny v usnesení zastupitelstva.

Důležitým bodem je **volba nového vedení**, protože stávajícímu výboru vypršel dvouletý mandát. Prosíme, nepodceňte nominace, které zasílejte na níže uvedenou adresu též ideálně do 2020-09-13. Nároky na kandidáty uvádí [bod 8.3 stanov](https://www.otevrenamesta.cz/stanovy#8.3).

Po ukončení členské schůze bude následovat prohlídka prostor školy doplněná veselými historkami z budování a provozu, naváže afterparty. Pro účely organizace nám, prosím, potvrďte předběžný zájem o tuto neformální část. Dotazy směřujte na e-mailovou adresu  [info@otevrenamesta.cz](mailto:info@otevrenamesta.cz).

Těšíme se na setkání!



[Seznam členů a jejich zástupců](http://www.otevrenamesta.cz/clenstvi)<br />
[Oficiální pozvánka](https://gitlab.com/otevrenamesta/documents/blob/master/schuze/2020_09_15/clenska_schuze-2020_09_15-pozvanka.pdf)


<p><img width="640" alt="ZŠ Amos, Dolní Jirčany" src="/assets/img/posts/clenska_2020_ZS_Amos_interier.jpg"></p>