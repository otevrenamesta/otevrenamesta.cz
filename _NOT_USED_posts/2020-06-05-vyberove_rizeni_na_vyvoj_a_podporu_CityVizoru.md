---
layout:       blog
image:        CV_screen.png
title:        Výběrové řízení na vývoj a podporu CityVizoru
hidden:       false
---
Otevřená města, z. s. vyhlašují prostřednictvím výboru výběrové řízení na vývoj a podporu aplikace [Cityvizor](https://cityvizor.cz/).

 <!--more-->

Aplikace:
- [Cityvizor](https://cityvizor.cz/) slouží jako transparentní dashboard obce a je postavený na otevřených datech o hospodaření měst a obcí, a to až do detailu jednotlivých faktur. Cílem projektu je nabídnout městům a obcím jednoduchou možnost vizualizovat data o hospodaření a dát je do kontextu takovým způsobem, aby byla srozumitelná pro širší veřejnost.

Obsah práce:
- správa aplikace Cityvizor: běžný provoz a řešení ad hoc problémů
- vývoj aplikace - implementace dohodnutých vylepšení (návody, fulltextové vyhledávání,…)
- řešení problémů s importem dat do aplikace, analytické práce, komunikace s obcemi

Požadujeme:
- TypeScript, Angular, NodeJS, Postgre (kódy na github.com/cityvizor/cityvizor)
- zkušenost se správou a vývojem webů ve výše zmíněných jazycích
- zkušenosti s GITem
- soběstačnost a přiměřenou časovou flexibilitu v případě urgentních záležitostí
- dobrá schopnost práce s daty, orientace v rozpočtech obcí vítána
- zkušenosti s vývojem OpenSource SW jsou výhodou
- minimální rozsah práce 30 hodin/měsíc

Nabízíme:
- odměna 500 Kč/h, práce na IČO
- skvělý kolektiv, který má snahu něco změnit
- práci z domova (komunikace přes JitsiMeet a Riot, výkazy práce přes Redmine)
- možnost podílet se na revoluci v transparentnosti obcí

Životopis a krátký motivační dopis nám zašlete do **20. 6. 2020** na **[cityvizor@otevrenamesta.cz](mailto:cityvizor@otevrenamesta.cz?subject=Výběrové řízení - vývoj a podpora CityVizoru)**. V následujícím týdnu se Vám ozveme ohledně dalšího postupu. Pro upřesnění informací se na nás neváhejte obrátit na cityvizor@otevrenamesta.cz nebo +420 737 412 935 (Pavla Kadlecová).