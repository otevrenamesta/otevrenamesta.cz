---
layout:       blog
image:        clenska_2020_online.jpg
title:        Členská schůze Otevřených měst &num;6 | online
hidden:       false
---
Zveme členy, zájemce o členství i ostatní příznivce na členskou schůzi, která proběhne v úterý **13.&nbsp;října&nbsp;2020** online.

 <!--more-->

Interaktivní část členské schůze se uskuteční v čase od **10:00 do 11:00** a proběhne **distančně** ([Jitsi](https://cs.wikipedia.org/wiki/Jitsi)). Hlasování o obsazení volených funkcí bude otevřeno **do 16:00** ([Tox](https://cs.wikipedia.org/wiki/Tox)).

**Hlavním bodem programu bude volba výboru, předsedy a kontrolora**, protože stávajícím voleným orgánům vypršel dvouletý mandát. Prosíme, nepodceňte nominace, které zasílejte na níže uvedenou adresu ideálně do 5. října 2020. Nároky na kandidáty uvádí [bod 8.3 stanov](https://www.otevrenamesta.cz/stanovy#8.3).

**Členské schůzi bude dne 7. října 2020 (od 9:00 do 11:00) předcházet prezentace všech nominovaných kandidátů na členy výboru.**

<p><img width="640" alt="Představení kandidátů" src="/assets/img/posts/clenska_2020_online.png"></p>

Členské schůze a prezentace všech nominovaných kandidátů se bude možné účastnit s využitím **videokonference** [Jitsi](https://en.wikipedia.org/wiki/Jitsi) na adrese [https://meet.vpsfree.cz/OM2020](https://meet.vpsfree.cz/OM2020).

**Vzdáleně hlasovat**:
- bude možné pouze pomocí [sítě Tox](https://cs.wikipedia.org/wiki/Tox)
- žádáme členy, kteří vzdálenou účast plánují, aby nejpozději do 12. října 2020 zaslali zprávou z oficiálního účtu datových schránek člena Tox ID, které zástupce použije
- klienta pro Tox můžete stáhnout na adrese [https://tox.chat/download.html](https://tox.chat/download.html)
- základní návod je k dispozici na adrese [https://cs.wikibooks.org/wiki/Tox](https://cs.wikibooks.org/wiki/Tox)

Také si dovolujeme upozornit na skutečnost, že hlasovat mohou pouze osoby, které doložily nebo nejpozději na členské schůzi doloží své oprávnění zastupovat člena na jednání spolku. Netýká se osob, které jsou zastupujícími dle Zákona o obcích (např. starosta, místostarosta, . . . ), nebo byly uvedeny v usnesení zastupitelstva.


Program jednání bude odsouhlasen účastníky na začátku schůze. Předpokládaný průběh je následující: 

bod | kdy | co | zpravodaj
--- | --: | --- |
|09:00|otevření online místnosti (Jitsi)|
|09:30|začátek registrace účastníků + technická podpora hlasování|
|09:55|konec registrace účastníků|
1|10:00|schválení programu schůze|členové
2|10:15|volba volených orgánů ([požadavky](https://www.otevrenamesta.cz/stanovy#8.3))|členové
|11:00|ukončení interaktivní části (Jitsi)|
|16:00|ukončení hlasování (Tox)|

Dotazy směřujte na e-mailovou adresu  [info@otevrenamesta.cz](mailto:info@otevrenamesta.cz).

Těšíme se na setkání!

[Seznam členů a jejich zástupců](http://www.otevrenamesta.cz/clenstvi)<br />
[Oficiální pozvánka](https://gitlab.com/otevrenamesta/documents/blob/master/schuze/2020_09_15/clenska_schuze-2020_10_13-pozvanka.pdf)
