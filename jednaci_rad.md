---
layout:       page
title:        Provizorní jednací řád
description:  Aktuální jednací řád
permalink:    /jednaci_rad/
id:           autocircles
---

**Otevřená města, z. s.**

Provizorní jednací řád členské schůze

K čl. 6.4.2 stanov

Členská schůze se na svém zasedání usnáší většinou hlasů těch členů, kteří se účastní hlasování, pokud byla pozvánka na zasedání zaslána všem členům spolku aspoň 15 dní předem a daný bod programu byl v pozvánce uveden. Výbor aspoň 15 dní před rozesláním pozvánky vyzve členy, aby zaslali svoje návrhy do programu a tyto návrhy uvede v programu v pozvánce.

K čl. 6.5 stanov

Výbor v pozvánce na jednání uvede, jakým způsobem mohou účastníci jednat vzdáleně po Internetu. Řešení musí být založeno na otevřených standardech. Účastníci musí mít možnost účastnit se jednání za použití výhradně svobodného software na všech úrovních řešení od operačního systému a výše. Řešení pro vzdálené hlasování musí umožnit ověření totožnosti hlasující osoby. Řešení musí být dostupné všem členům bez dodatečných poplatků za užití.

Ustanovení o platnosti

Tento provizorní jednací řád nabývá účinnosti dnem následujícím po jeho schválení členskou schůzí.
